#include <iostream>
#include <algorithm>
#include "MyGraph.h"
#include "InitSol.h"


int f_forest(bool*, MyGraph);
int neighbours_forest(bool*, MyGraph, bool**, int);
int max_f_forest(bool*, MyGraph, int, int);
int is_tabu_forest(bool*, bool**, int, MyGraph);
int tabu_forest(bool * , MyGraph , int , int , bool * );
