PROG = test
CC = g++
CFLAGS = -g -Wall
OBJS = MyGraph.o InitSol.o TabuForest.o TabuTree.o TabuBipartite2.o





test: clean $(OBJS)
	$(CC) $(CFLAGS) -o test $(OBJS) test.cpp

experiment: clean $(OBJS)
		$(CC) $(CFLAGS) -o expe $(OBJS) Expe.cpp
		./expe

MyGraph.o: MyGraph.h
	$(CC) $(CFLAGS) -c MyGraph.cpp

InitSol.o: InitSol.h
	$(CC) $(CFLAGS) -c InitSol.cpp


TabuForest.o: TabuForest.h
	$(CC) $(CFLAGS) -c TabuForest.cpp

TabuTree.o: TabuTree.h
		$(CC) $(CFLAGS) -c TabuTree.cpp

TabuBipartite.o: TabuBipartite.h
				$(CC) $(CFLAGS) -c TabuBipartite.cpp

TabuBipartite2.o: TabuBipartite2.h
								$(CC) $(CFLAGS) -c TabuBipartite2.cpp


runtest: clean test
	./test


clean :
	rm -f $(PROG) $(OBJS)
