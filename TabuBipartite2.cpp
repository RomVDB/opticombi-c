#include "TabuBipartite2.h"
/*
*
*/
int f_bipartite2(int * s , MyGraph G){
  bool* subgraph = new bool[G.getn()];
  for(int i=0;i <G.getn(); i++){
    if(s[i] != 0){
      subgraph[i] = 1;
    }
    else{
      subgraph[i] = 0;
    }
  }
  if (G.isBipartite(subgraph) == false ){
    return -1;
  }
  else{
    int count = 0;
    for(int i=0; i<G.getn(); i++){
      if(s[i] != 0){
        count ++;
      }
    }
    return count;
  }
}




/*
*
*/
int neighbours_bipartite2(int* s, MyGraph G, int** neigh, int snb){
  // Color an uncolored vertex, decolor conflicting neighbours
  int glob_count = 0;
  for(glob_count=0; glob_count<G.getn()-snb;glob_count++){
    for(int i=0;i<G.getn(); i++){
      (neigh)[glob_count][i] = s[i];
    }
    int line_counter = 0;
    int flag = true;
    for(int i=0;i<G.getn() && flag; i++){
      if(s[i] == 0){
        if(line_counter == glob_count){
          flag = false;
          // color this vertex
          int flag1 = false;
          int count1 = 0;
          int flag2 = false;
          int count2 = 0;
          bool** A = G.getadjmatrix();
          for(int j=0;j<G.getn(); j++){
            if(A[i][j] == 1){
              // vertex j is adjacent
              if(s[j] == 1){
                flag1 = true;
                count1 ++;
              }
              else if(s[j] == 2){
                flag2 = true;
                count2 ++;
              }
            }
          }

          if(!flag1){
            // color in 1, no conflict
            (neigh)[glob_count][i] = 1;
          }
          else if(!flag2){
            // color in 2, no conflict
            (neigh)[glob_count][i] = 2;
          }
          else if(count2 > count1){
            // color in 1, less conflict
            (neigh)[glob_count][i] = 1;
            for(int j=0;j<G.getn(); j++){
              if(A[i][j] == 1){
                // vertex j is adjacent
                if(s[j] == 1){
                  // decolor conflicting vertex
                  (neigh)[glob_count][j] = 0;
                }
              }
            }
          }
          else{
            // color in 2, less conflict
            (neigh)[glob_count][i] = 2;
            for(int j=0;j<G.getn(); j++){
              if(A[i][j] == 1){
                // vertex j is adjacent
                if(s[j] == 2){
                  // decolor conflicting vertex
                  (neigh)[glob_count][j] = 0;
                }
              }
            }
          } // color choice
        } // if line_counter == glob_count
        else{
          line_counter ++;
        } // if line_counter == glob_count else
      } // if s[i] == 0
    } // for i (line)
  } // for glob_count


  return 0;
}




/*
*
*/
int max_f_bipartite2(int* s, MyGraph G, int fs, int** T, int nbtabu, int * smax){
  int n = G.getn();
  //int * smax = new int[n];
  int fmax = fs;
  int snb =0; // number of colored vertices
  for(int i=0; i<n;i++){
    if(s[i] != 0){
      snb ++;

    }
  }
  int** neigh = (int**) malloc(n * sizeof(int*));
  if(neigh == NULL){ printf("Error allocation memory\n"); return -1;}
  for(int i=0; i< n;i++){
    neigh[i] = (int*) malloc(n* sizeof(int));
    if(neigh[i] == NULL){ printf("Error allocation memory\n"); return -1;}
    for(int j=0;j<n;j++){
      neigh[i][j] = 0;
    }
  }
  neighbours_bipartite2(s, G, neigh, snb);




  /****************************************************************************
  * Neighbours
  ****************************************************************************/


  /****************************************************************************
  * Neighbours
  ****************************************************************************/
  int fbar;
  int k;
  int flag = true;
  for(k=0; k<n; k++){
    if(!is_tabu_bipartite2(neigh[k], T, nbtabu, G)){
      // not tabu
      // Evaluate and compare to best solution so far
      fbar = f_bipartite2(neigh[k], G);
      if(fbar > fmax || flag){
        // Better solution found or copy first one
        flag = false;
        fmax = fbar;
        for(int i=0; i<n; i++){
          smax[i] = neigh[k][i];
        }
      }
    }
  }



  for(int i=0; i< n;i++){
    free(neigh[i]);
  }
  free(neigh);
  return fmax;


}


/*
*
*/
int is_tabu_bipartite2(int* s, int** T, int nbtabu, MyGraph G){
  int break_flag = true;
  for(int i=0; i< nbtabu; i++){
    break_flag = true;
    for(int j=0; j<G.getn() && break_flag;j++){
      if(s[j] != T[i][j]){
        break_flag = false;
      }
    }
    if(break_flag){
      return true;
    }
  }
  return false;
}




/*
*
*/
int tabu_bipartite2(int * s_start, MyGraph G, int kmax, int nbtabu, int * s_star){
  // Initialization
  int k;
  int n = G.getn();
  int ** T = new int*[nbtabu];
  for(int i=0; i<nbtabu; i++){
    T[i] = new int[n];
    for(int j = 0; j<n;j++){
      T[i][j] = 0;
    }
  }
  int f_star = f_bipartite2(s_start, G);
  int* s_prime = new int[n];
  int* old = new int[n];
  int f_prime = 0;
  for(int i=0; i<n; i++){
    s_star[i] = s_start[i];
    old[i] = s_start[i];
    s_prime[i] = 0;
  }

  // Iteration
  //printf("Iteration %d : f_star = %d\n", 0, f_star);
  for(k=0; k < kmax; k++){

    f_prime =  max_f_bipartite2(old, G, f_prime, T, nbtabu, s_prime);
    //printf("Iteration %d : f_star = %d and current is %d\n", k, f_star, f_prime);
    if(f_prime > f_star){
      // Success : update s_star and s_prime
      f_star = f_prime;
      for(int i=0; i<n; i++){
        s_star[i] = s_prime[i];
      }
    }

    // Add s_sprime in Tabu list
    for(int i=0; i<n; i++){
      T[k%nbtabu][i] = s_prime[i];
      old[i] = s_prime[i];
    }

  }
  return f_star;
}
