#include "TabuForest.h"



int f_forest(bool * s, MyGraph G){
  int ret = G.isCyclic(s);
  if (ret >= 0){
    printf("Cycle found\n");
    return -1;
  }
  else{
    int count = 0;
    for(int i=0; i<G.getn(); i++){
      if(s[i] == 1){
        count ++;
      }
    }
    return count;
  }
}

/*

int neighbours_forest(int* s, MyGraph G, int** neigh, int snb){
  // Remove a vertex in subgraph
  int glob_count = 0;
  for(glob_count=0; glob_count<snb;glob_count++){
    int line_counter = 0;
    for(int i=0; i<G.getn();i++){
      if(s[i] == 1){
        if(line_counter == glob_count){
          neigh[glob_count][i] = 0;
        }
        else{
          neigh[glob_count][i] = s[i];
        }
        line_counter++;
      }
      else{
        neigh[glob_count][i] = s[i];
      }
    }
  }
  // Add a vertex in subgraph
  glob_count = 0;
  for(glob_count=0; glob_count<G.getn()-snb ;glob_count++){
    int line_counter = 0;
    for(int i=0; i<G.getn();i++){
      if(s[i] == 0){
        if(line_counter == glob_count){
          neigh[snb+glob_count][i] = 1;
        }
        else{
          neigh[glob_count+snb][i] = s[i];
        }
        line_counter++;
      }
      else{
        neigh[glob_count+snb][i] = s[i];
      }
    }
  }

  return 0;
}



*/

/*
* Compute the neighbours of a solution :
* Add vertex in subgraph and remove adjacent vertices which create cycles
*
*
*/
int neighbours_forest(bool* s, MyGraph G, bool** neigh, int snb){
  // Add vertex and remove conflicting adjacent vertices
  int glob_count = 0;
  for(glob_count=0; glob_count<G.getn()-snb;glob_count++){
    // copy current solution
    for(short i=0;i<G.getn(); i++){
      neigh[glob_count][i] = s[i];
    }
    // Count to identify vertex to be added
    short line_counter = 0;
    bool flag = true;
    for(int i=0;i<G.getn() && flag; i++){
      if(s[i] == 0){
        if(line_counter == glob_count){
        //  Add vertex i in subgraph
          flag = false;
          neigh[glob_count][i] = 1;
          // Check conflict, starting from added vertex
          bool** A = G.getadjmatrix();
          int cycle = G.isCyclic(neigh[glob_count], i);
          for(int k=0; k< G.getn() &&  cycle != -1; k++){
            //There are conflict
            if(A[i][k] == 1){
              //remove vertex cycle
              neigh[glob_count][cycle] = 0;
            }
            cycle = G.isCyclic(neigh[glob_count], i);
          }
        } // if line_counter == glob_count
        else{
          line_counter ++;
        } // if line_counter == glob_count else
      } // if s[i] == 0
    } // for i (line)
  } // for glob_count
  return 0;
}


int max_f_forest(bool* s, MyGraph G, int fs, bool** T, int nbtabu, bool * smax){

  int n = G.getn();
  //int * smax = new int[n];
  int fmax = fs;
  short nbs =0;
  for(int i=0; i<n;i++){
    if(s[i] != 0){
      nbs ++;
    }
  }
  bool** neigh = (bool**) malloc(n * sizeof(bool*));
  if(neigh == NULL){ printf("Error allocation memory\n"); return -1;}
  for(int i=0; i< n;i++){
    neigh[i] = (bool*) malloc(n* sizeof(bool));
    if(neigh[i] == NULL){ printf("Error allocation memory\n"); return -1;}
    for(int j=0;j<n;j++){
      neigh[i][j] = 0;
    }
  }

  neighbours_forest(s, G, neigh, nbs);
  int fbar;
  int k;
  bool flag = true;
  for(k=0; k<n ; k++){

    if(!(is_tabu_forest(neigh[k], T, nbtabu, G))){
      // not tabu
      fbar = f_forest(neigh[k], G);
      if(fbar > fmax || flag){
        // Better solution found or copy first one
        flag = false;
        fmax = fbar;
        for(int i=0; i<n; i++){
          smax[i] = neigh[k][i];
        }
      }
    }
  }
  for(int i=0;i<n;i++){
    free(neigh[i]);
  }
  free(neigh);
  return fmax;
}


int is_tabu_forest(bool* s, bool** T, int nbtabu, MyGraph G){
  bool break_flag = true;
  int sum = 0;
  for(int i=0; i<G.getn(); i++){
    if(s[i]){
      sum ++;
    }
  }
  if (sum == 0){return true;}
  for(short i=0; i< nbtabu; i++){
    break_flag = true;
    for(short j=0; j<G.getn() && break_flag;j++){
      if(s[j] != T[i][j]){
        break_flag = false;
      }
    }
    if(break_flag){
      return true;
    }
  }
  return false;
}



int tabu_forest(bool * s_start, MyGraph G, int kmax, int nbtabu, bool * s_star){
  // Initialization
  int k;
  short n = G.getn();
  //int ** T = new int*[nbtabu];
  bool ** T = (bool **) malloc(nbtabu * sizeof(bool *));
  if(T == NULL){ return -1;}
  for(int i=0; i<nbtabu; i++){
    //T[i] = new int[n];
    T[i] = (bool *) malloc(n * sizeof(bool));
    if(T[i] == NULL){ return -1;}
    for(int j=0; j < n; j++){
      T[i][j] = 0;
    }
  }
  int f_star = f_forest(s_start, G);
  //bool ** neigh = new bool*[n];

  bool* s_prime = (bool *) malloc( n* sizeof(bool));
  if(s_prime == NULL){ return -1;}

  bool* old = (bool *) malloc( n* sizeof(bool));
  if(old == NULL){ return -1;}


  int f_prime = 0;
  for(short i=0; i<n; i++){
    s_star[i] = s_start[i];
    old[i] = s_start[i];
    s_prime[i] = 0;
  }

  // Iteration
  //printf("Iteration %d : f_star = %d\n", 0, f_star);
  for(k=0; k < kmax; k++){

    f_prime =  max_f_forest(old, G, f_prime, T, nbtabu, s_prime);
    printf("Iteration %d : f_star = %d and current is %d\n", k, f_star, f_prime);
    if(f_prime > f_star){
      // Success : update s_star and s_prime
      f_star = f_prime;
      for(int i=0; i<n; i++){
        s_star[i] = s_prime[i];
      }
    }

    // Add s_sprime in Tabu list
    for(int i=0; i<n; i++){
      T[k%nbtabu][i] = s_prime[i];
      old[i] = s_prime[i];
    }

  }

  free(s_prime);
  free(old);
  for(int i=0; i< nbtabu; i++){
    free(T[i]);
  }
  free(T);
  return f_star;
}
