#include <iostream>
#include <algorithm>
#include "MyGraph.h"
#include "InitSol.h"



int f_bipartite(int*, MyGraph);
int neighbours_bipartite(int*, MyGraph, int**, int);
int max_f_bipartite(int*, MyGraph, int**, int,   int**, int);
int is_tabu_bipartite(int*, int**, int, MyGraph);
int tabu_bipartite(int * , MyGraph , int , int , int * );
