#include "Expe.h"







/*
*
*
*/
int experiment(char* filename, int nb_iteration, int nb_repetion, int tabulen){
  MyGraph G = MyGraph(0);
  int m;
  FILE* fp = fopen(filename, "r");
  if (fp == NULL){
    printf("Could not open file %s\n", filename);
    return -1;
  }

  char* line = NULL;
  size_t len = 0;
  while ((getline(&line, &len, fp)) != -1) {
    // using printf() in all tests for consistency
    if(line[0] == 'p'){
      // create graph
      char ** into = new char*[4];
      into[0] = new char[2];
      into[1] = new char[5];
      into[2] = new char[5];
      into[3] = new char[10];
      split(line, ' ', into);
      G =  MyGraph(atoi(into[2]));
      m = atoi(into[3]);
    }
    else if(line[0] == 'e'){
      // add edge
      char ** into = new char*[3];
      into[0] = new char[2];
      into[1] = new char[5];
      into[2] = new char[5];
      split(line,' ', into);
      G.add_edge(atoi(into[1]), atoi(into[2]));
    }
  }
  fclose(fp);
  if (line){
    free(line);
  }

  //printf("number of edges is %d, should be %d\n",G.getm(),m);




  int n = G.getn();
  double avg_init = 0.0;
  int worst = n;
  int best = -1;
  double avg_sol = 0.0;
  int f_star = 0;
  double avg_time = 0.0;


    /*************************************************************/
    // Forest


/*
  for(int repet = 0; repet < nb_repetion; repet ++){
    printf("Problem forest : repetition %d on  %s\n", repet, filename);
    bool* s_star = new bool [n];
    init_forest(G);
    bool * s_start = new bool[n];
    bool * tmp = G.getsubgraph();
    for(int i=0; i<n;i++){
      if(tmp[i] != 0){
        avg_init ++;
      }
      s_start[i] = tmp[i];
      s_star[i] = 0;
    }
    auto start = high_resolution_clock::now();
    f_star = tabu_forest(s_start, G,nb_iteration, tabulen, s_star);
    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<microseconds>(stop - start);
    avg_time = avg_time +  (double)  duration.count();
    // Record data
    avg_sol = avg_sol + (double) f_star;
    if(f_star < worst){
      worst = f_star;
    }
    if(f_star > best){
      best = f_star;
    }
    printf("%s\t init : %f, worst : %d, best : %d, avg : %f, time : %f\n", filename, avg_init, worst, best, avg_sol, avg_time);
  }

  avg_init = avg_init/ (double) nb_repetion;
  avg_sol = avg_sol/ (double) nb_repetion;
  avg_time = avg_time/ (double) nb_repetion;

  // write in file
  ofstream myfile;
  std::ostringstream out;
  myfile.open("Results_forest_bis.txt", std::ios_base::app);

  out << "iteration = " << nb_iteration << "; repetition = " << nb_repetion <<"\n" << filename << " & " << avg_init << " & " << worst << " & " << best << " & " << avg_sol << " & " << avg_time << "\n";
  myfile << out.str() << " \n";
  myfile.close();
  std::cout<< out.str();
*/


  /***************************************************************************/
  // Tree

  avg_init = 0.0;
  worst = n;
  best = -1;
  avg_sol = 0.0;
  f_star = 0;
  avg_time = 0.0;

  for(int repet = 0; repet < nb_repetion; repet ++){
    printf("Problem tree : repetition %d on  %s\n", repet, filename);
    bool* s_star = new bool [n];
    init_tree(G);
    bool * s_start = new bool[n];
    bool * tmp = G.getsubgraph();
    for(int i=0; i<n;i++){
      if(tmp[i] != 0){
        avg_init ++;
      }
      s_start[i] = tmp[i];
      s_star[i] =0;
    }
    auto start = high_resolution_clock::now();
    f_star = tabu_tree(s_start, G,nb_iteration, tabulen, s_star);
    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<microseconds>(stop - start);
    avg_time = avg_time +  (double)  duration.count();
    // Record data
    avg_sol = avg_sol +  (double) f_star;
    if(f_star < worst){
      worst = f_star;
    }
    if(f_star > best){
      best = f_star;
    }
    printf("%s\t init : %f, worst : %d, best : %d, avg : %f, time : %f\n", filename, avg_init, worst, best, avg_sol, avg_time);
  }

  avg_init = avg_init/ (double) nb_repetion;
  avg_sol = avg_sol/ (double) nb_repetion;
  avg_time = avg_time/ (double) nb_repetion;

  // write in file
  ofstream myfile1;
  std::ostringstream out1;
  myfile1.open("Results_tree_bis.txt", std::ios_base::app);
  out1<< "iteration = " << nb_iteration << "; repetition = " << nb_repetion <<"\n" << filename << " & " << avg_init << " & " << worst << " & " << best << " & " << avg_sol << " & " << avg_time << " \n";
  myfile1 << out1.str() << " \n";
  myfile1.close();
  std::cout<< out1.str();




  /***************************************************************************/
  // Bipartite
  /*
  avg_init = 0.0;
  worst = n;
  best = -1;
  avg_sol = 0.0;
  f_star = 0;
  avg_time = 0.0;

  for(int repet = 0; repet < nb_repetion; repet ++){
    printf("Problem bipartite : repetition %d on %s\n", repet, filename);
    int* s_star = new int [n];
    int * s_start = new int[n];
    for(int i=0; i<n;i++){
      s_start[i] = 0;
      s_star[i] = 0;
    }
    init_bipartite(G,s_start);
    for(int i=0; i<n;i++){
      if(s_start[i] != 0){
        avg_init ++;
      }
    }
    auto start = high_resolution_clock::now();
    f_star = tabu_bipartite2(s_start, G,nb_iteration, tabulen, s_star);
    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<microseconds>(stop - start);
    avg_time = avg_time +  (double)  duration.count();
    // Record data
    avg_sol = avg_sol +  (double)  f_star;
    if(f_star < worst){
      worst = f_star;
    }
    if(f_star > best){
      best = f_star;
    }
    printf("%s\t init : %f, worst : %d, best : %d, avg : %f, time : %f\n", filename, avg_init, worst, best, avg_sol, avg_time);
  }

  avg_init = avg_init/ (double) nb_repetion;
  avg_sol = avg_sol/ (double) nb_repetion;
  avg_time = avg_time/ (double) nb_repetion;

  // write in file
  ofstream myfile2;
  std::ostringstream out2;
  myfile2.open("Results_bipartite_bis.txt", std::ios_base::app);
  out2<< "iteration = " << nb_iteration << "; repetition = " << nb_repetion <<"\n" << filename << " & " << avg_init << " & " << worst << " & " << best << " & " << avg_sol << " & " << avg_time << "\n\n";
  myfile2 << out2.str();
  myfile2.close();
  std::cout<< out2.str();
  */

  return 0;
}


/*****************************************************************************/
/*****************************************************************************/
/*****************************************************************************/
/*****************************************************************************/
/*****************************************************************************/


void complete_experiment(char *filename, int problem, int nb_iteration, int tabulen, int* result){
  if(problem < 0 || problem >3){
    return;
  }
  MyGraph G = MyGraph(0);
  int m;
  FILE* fp = fopen(filename, "r");
  if (fp == NULL){
    printf("Could not open file %s\n", filename);
    return;
  }

  char* line = NULL;
  size_t len = 0;
  while ((getline(&line, &len, fp)) != -1) {
    // using printf() in all tests for consistency
    if(line[0] == 'p'){
      // create graph
      char ** into = new char*[4];
      into[0] = new char[2];
      into[1] = new char[5];
      into[2] = new char[5];
      into[3] = new char[10];
      split(line, ' ', into);
      G =  MyGraph(atoi(into[2]));
      m = atoi(into[3]);
    }
    else if(line[0] == 'e'){
      // add edge
      char ** into = new char*[3];
      into[0] = new char[2];
      into[1] = new char[5];
      into[2] = new char[5];
      split(line,' ', into);
      G.add_edge(atoi(into[1]), atoi(into[2]));
    }
  }
  fclose(fp);
  if (line){
    free(line);
  }

  int n=G.getn();
  if(problem ==1){
    // Forest

    bool* s_star = new bool [n];
    init_forest(G);
    bool * s_start = new bool[n];
    bool * tmp = G.getsubgraph();
    int finit = 0;
    int time=0;
    for(int i=0; i<n;i++){
      if(tmp[i] != 0){
        finit ++;
      }
      s_start[i] = tmp[i];
      s_star[i] = 0;
    }
    auto start = high_resolution_clock::now();
    int f_star = tabu_forest(s_start, G,nb_iteration, tabulen, s_star);
    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<microseconds>(stop - start);
    time =   (int)  duration.count();
    // Record data

    result[0] = finit;
    result[1] = f_star;
    result[2] = time;

  }
  else if(problem == 2){
    //tree

    bool* s_star = new bool [n];
    init_tree(G);
    bool * s_start = new bool[n];
    bool * tmp = G.getsubgraph();
    int finit = 0;
    int time=0;
    for(int i=0; i<n;i++){
      if(tmp[i] != 0){
        finit ++;
      }
      s_start[i] = tmp[i];
      s_star[i] = 0;
    }
    auto start = high_resolution_clock::now();
    int f_star = tabu_tree(s_start, G,nb_iteration, tabulen, s_star);
    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<microseconds>(stop - start);
    time =   (int)  duration.count();
    // Record data

    result[0] = finit;
    result[1] = f_star;
    result[2] = time;
  }
  else if(problem == 3){
    //Bipartite

    int* s_star = new int [n];
    int * s_start = new int[n];
    int finit = 0;
    int time=0;
    for(int i=0; i<n;i++){
      s_start[i] = 0;
      s_star[i] = 0;
    }
    init_bipartite(G,s_start);
    for(int i=0; i<n;i++){
      printf("%d\t",s_start[i]);
      if(s_start != 0){
        finit ++;
      }
    }
    printf("\n");


    auto start = high_resolution_clock::now();
    int f_star = tabu_bipartite2(s_start, G,nb_iteration, tabulen, s_star);
    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<microseconds>(stop - start);
    time = (int)  duration.count();
    // Record data
    result[0] = finit;
    result[1] = f_star;
    result[2] = time;

  }
  else{
    printf("Undefined Problem\n");
  }
}






/*****************************************************************************/
/*****************************************************************************/
/*****************************************************************************/
/*****************************************************************************/
/*****************************************************************************/


/*****************************************************************************
* main
******************************************************************************/

int main(int argc, char const *argv[]) {
  int nb_iteration = 100;
  int nb_repetion = 1;
  int tabulen = 100;

  //experiment("Python/Instances/DSJC125.1.txt", nb_iteration, nb_repetion, tabulen);
  //experiment("Python/Instances/DSJC125.5.txt", nb_iteration, nb_repetion, tabulen);
  //experiment("Python/Instances/DSJC125.9.txt", nb_iteration, nb_repetion, tabulen);

  //experiment("Python/Instances/DSJC250.1.txt", nb_iteration, nb_repetion, tabulen);
  //experiment("Python/Instances/DSJC250.5.txt", nb_iteration, nb_repetion, tabulen);
  //experiment("Python/Instances/DSJC250.9.txt", nb_iteration, nb_repetion, tabulen);


  //experiment("Python/Instances/DSJC500.1.txt", nb_iteration, nb_repetion, tabulen);
  //experiment("Python/Instances/DSJC500.5.txt", nb_iteration, nb_repetion, tabulen);
  //experiment("Python/Instances/DSJC500.9.txt", nb_iteration, nb_repetion, tabulen);

  experiment("Python/Instances/DSJC1000.1.txt", nb_iteration, nb_repetion, tabulen);
  //experiment("Python/Instances/DSJC1000.5.txt", nb_iteration, nb_repetion, tabulen);
  //experiment("Python/Instances/DSJC1000.9.txt", nb_iteration, nb_repetion, tabulen);


char* filename = "Python/Instances/DSJC1000.1.txt";


  MyGraph G = MyGraph(0);
  int m;
  FILE* fp = fopen(filename, "r");
  if (fp == NULL){
    printf("Could not open file %s\n", filename);
    return -1;
  }

  char* line = NULL;
  size_t len = 0;
  while ((getline(&line, &len, fp)) != -1) {
    // using printf() in all tests for consistency
    if(line[0] == 'p'){
      // create graph
      char ** into = new char*[4];
      into[0] = new char[2];
      into[1] = new char[5];
      into[2] = new char[5];
      into[3] = new char[10];
      split(line, ' ', into);
      G =  MyGraph(atoi(into[2]));
      m = atoi(into[3]);
    }
    else if(line[0] == 'e'){
      // add edge
      char ** into = new char*[3];
      into[0] = new char[2];
      into[1] = new char[5];
      into[2] = new char[5];
      split(line,' ', into);
      G.add_edge(atoi(into[1]), atoi(into[2]));
    }
  }
  fclose(fp);
  if (line){
    free(line);
  }

  //printf("number of edges is %d, should be %d\n",G.getm(),m);




  int n = G.getn();
  double avg_init = 0.0;
  int worst = n;
  int best = -1;
  double avg_sol = 0.0;
  int f_star = 0;
  double avg_time = 0.0;


    /*************************************************************/
    // Forest



  for(int repet = 0; repet < nb_repetion; repet ++){
    printf("Problem forest : repetition %d on  %s\n", repet, filename);
    bool* s_star = new bool [n];
    init_forest(G);
    bool * s_start = new bool[n];
    bool * tmp = G.getsubgraph();
    for(int i=0; i<n;i++){
      if(tmp[i] != 0){
        avg_init ++;
      }
      s_start[i] = tmp[i];
      s_star[i] = 0;
    }
    auto start = high_resolution_clock::now();
    f_star = tabu_forest(s_start, G,nb_iteration, tabulen, s_star);
    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<microseconds>(stop - start);
    avg_time = avg_time +  (double)  duration.count();
    // Record data
    avg_sol = avg_sol + (double) f_star;
    if(f_star < worst){
      worst = f_star;
    }
    if(f_star > best){
      best = f_star;
    }
    printf("%s\t init : %f, worst : %d, best : %d, avg : %f, time : %f\n", filename, avg_init, worst, best, avg_sol, avg_time);
  }

  avg_init = avg_init/ (double) nb_repetion;
  avg_sol = avg_sol/ (double) nb_repetion;
  avg_time = avg_time/ (double) nb_repetion;

  // write in file
  ofstream myfile;
  std::ostringstream out;
  myfile.open("Results_forest_bis.txt", std::ios_base::app);

  out << "iteration = " << nb_iteration << "; repetition = " << nb_repetion <<"\n" << filename << " & " << avg_init << " & " << worst << " & " << best << " & " << avg_sol << " & " << avg_time << "\n";
  myfile << out.str() << " \n";
  myfile.close();
  std::cout<< out.str();






  printf("Experiment ended\n");
  return 0;
}

















/*****************************************************************************
* Helper functions
******************************************************************************/

void split(const char* str, const char d, char** into)
{
    if(str != NULL && into != NULL)
    {
        int n = 0;
        int c = 0;
        for(int i = 0; str[c] != '\0'; i++,c++)
        {
            into[n][i] = str[c];
            if(str[c] == d)
            {
                into[n][i] = '\0';
                i = -1;
                ++n;
            }
        }
    }
}
