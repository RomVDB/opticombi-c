#include "InitSol.h"
#include "MyGraph.h"


int init_forest(MyGraph G){
  int n = G.getn();
  bool * sb = G.getsubgraph();
  int * tab = new int[n];
  for(int i=0;i<n;i++){
    sb[i] = 0;
    tab[i] = i;
  }
  shuffle_tab(tab, n);
  for(int i=0;i<n;i++){
    sb[tab[i]] = 1;
    if(G.isCyclic(1) != -1){
      sb[tab[i]] = 0;
    }
  }

  return 0;
}

int init_bipartite(MyGraph G, int* col){
  int n = G.getn();
  bool * sb = G.getsubgraph();
  int * tab = new int[n];
  for(int i=0;i<n;i++){
    sb[i] = 0;
    tab[i] = i;
  }
  shuffle_tab(tab, n);
  for(int i=0;i<n;i++){
    sb[tab[i]] = 1;
    if(G.isBipartite(sb, col) == false){
      sb[tab[i]] = 0;
    }
  }
  for(int i=0; i< G.getn(); i++){
    if(sb[i] != 0){
      col[i] = 1+col[i];
    }
  }
  return 0;
}


int init_tree(MyGraph G){
  int n = G.getn();
  bool * sb = G.getsubgraph();
  int * tab = new int[n];
  for(int i=0;i<n;i++){
    sb[i] = 0;
    tab[i] = i;
  }
  shuffle_tab(tab, n);
  for(int i=0;i<n;i++){
    sb[tab[i]] = 1;
    if(G.isConnected(1) == false || G.isCyclic(1) != -1){
      sb[tab[i]] = 0;
    }
  }
  return 0;
}



int shuffle_tab(int * tab, int n){;
  int i;
  int m = n;
  int * newtab = new int[n];
  //int * rettab = new int[n];
  for(i = 0; i< n; i++){
    newtab[i] = tab[i];
  }
  for(i = 0; i< n; i++){

    int ind = rand() % m;
    m --;
    tab[i] = newtab[ind];
    for(int j=ind;j<m;j++){
      newtab[j] = newtab[j +1];
    }
  }
  return 0;
}
