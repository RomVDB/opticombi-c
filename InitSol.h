#include <iostream>
#include <algorithm>
#include "MyGraph.h"



int shuffle_tab(int *, int);
int init_forest(MyGraph );
int init_bipartite(MyGraph, int* );
int init_tree(MyGraph );
