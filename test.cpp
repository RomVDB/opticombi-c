#include <iostream>
#include <fstream>
#include "MyGraph.h"
#include "TabuForest.h"
#include "TabuTree.h"
#include "TabuBipartite.h"
#include "TabuBipartite2.h"
#include "InitSol.h"

using namespace std;




/******************************************************************************
*
*
*
*       Test MyGraph
*
*
*
*******************************************************************************/

/*
void test_subgraph(){
  MyGraph G =  MyGraph(8);
  G.add_edge(1,2);
  G.add_edge(3,2);
  G.add_edge(1,3);
  G.add_edge(4,2);
  G.add_edge(4,5);
  G.add_edge(5,3);
  G.add_edge(6,7);
  G.add_edge(8,7);
  G.add_edge(6,8);
  G.add_edge(5,6);
  printf(" n = %d, should be 8 \n",G.getn());
  printf(" m = %d, should be 9 \n",G.getm());
  printf("Connected :\t Should be 1 : %d\n",G.isConnected(0));
  printf("Cycle :\t Should not be -1 : %d\n",G.isCyclic(0));
  printf("Bipartite :\t Should be 0 : %d\n",G.isBipartite(0));

  printf("Connected :\t Should be 1 : %d\n",G.isConnected(1));
  printf("Cycle :\t Should be -1 : %d\n",G.isCyclic(1));
  printf("Bipartite :\t Should be 1 : %d\n",G.isBipartite(1));

  for(int i=0;i<G.getn();i++){
    G.subgraph_addvertex(i+1);
  }
  printf("Connected :\t Should be 1 : %d\n",G.isConnected(1));
  printf("Cycle :\t Should not be -1: %d\n",G.isCyclic(1));
  printf("Bipartite :\t Should be 0 : %d\n",G.isBipartite(1));

  G.subgraph_removevertex(5);
  printf("Connected :\t Should be 0 : %d\n",G.isConnected(1));
  printf("Cycle :\t Should not be -1 : %d\n",G.isCyclic(1));
  printf("Bipartite :\t Should be 0 : %d\n",G.isBipartite(1));

  G.subgraph_removevertex(1);
  printf("Connected :\t Should be 0 : %d\n",G.isConnected(1));
  printf("Cycle :\t Should not be -1 : %d\n",G.isCyclic(1));
  printf("Bipartite :\t Should be 0 : %d\n",G.isBipartite(1));

  G.subgraph_removevertex(7);
  printf("Connected :\t Should be 0 : %d\n",G.isConnected(1));
  printf("Cycle :\t Should be -1 : %d\n",G.isCyclic(1));
  printf("Bipartite :\t Should be 1 : %d\n",G.isBipartite(1));

}

*/

/******************************************************************************
*
*
*
*       Test InitSol
*
*
*
*******************************************************************************/


/*

void test_initial_forest(){
  MyGraph G =  MyGraph(8);
  G.add_edge(1,2);
  G.add_edge(3,2);
  G.add_edge(1,3);
  G.add_edge(4,2);
  G.add_edge(4,5);
  G.add_edge(5,3);
  G.add_edge(6,7);
  G.add_edge(8,7);
  G.add_edge(6,8);
  //G.add_edge(5,6);

  init_forest(G);
  int * sb = G.getsubgraph();

  for(int i=0; i < G.getn(); i++){
    printf("subgraph[%d] = %d\n", i,sb[i]);
  }

}
*/

void test_initial_bipartite(){
  MyGraph G =  MyGraph(8);
  G.add_edge(1,2);
  G.add_edge(3,2);
  G.add_edge(1,3);
  G.add_edge(4,2);
  G.add_edge(4,5);
  G.add_edge(5,3);
  G.add_edge(6,7);
  G.add_edge(8,7);
  G.add_edge(6,8);
  //G.add_edge(5,6);
  int * col = new int[8];
  for(int i=0; i<8; i++){
    col[i] = 0;
  }
  init_bipartite(G, col);
  bool * sb = G.getsubgraph();

  for(int i=0; i < G.getn(); i++){
    printf("subgraph[%d] = %d\t", i,sb[i]);
    printf("col[%d] = %d\n", i,col[i]);
  }



}
/*
void test_initial_tree(){
  MyGraph G =  MyGraph(8);
  G.add_edge(1,2);
  G.add_edge(3,2);
  G.add_edge(1,3);
  G.add_edge(4,2);
  G.add_edge(4,5);
  G.add_edge(5,3);
  G.add_edge(6,7);
  G.add_edge(8,7);
  G.add_edge(6,8);
  //G.add_edge(5,6);

  init_tree(G);
  int * sb = G.getsubgraph();

  for(int i=0; i < G.getn(); i++){
    printf("subgraph[%d] = %d\n", i,sb[i]);
  }

}
*/

/******************************************************************************
*
*
*
*       Test TabuForest
*
*
*
*******************************************************************************/
/*

void test_f_forest(){
  MyGraph G =  MyGraph(8);
  G.add_edge(1,2);
  G.add_edge(3,2);
  G.add_edge(1,3);
  G.add_edge(4,2);
  G.add_edge(4,5);
  G.add_edge(5,3);
  G.add_edge(6,7);
  G.add_edge(8,7);
  G.add_edge(6,8);

  int* sb = new int[8];
  int fsb = f_forest(sb, G);
  int flag = true;
  if (fsb != 0){
    flag = false;
    printf("Error : f returns %d for empty subgraph\n", fsb);
  }
  sb[0] = 1;
  fsb = f_forest(sb, G);
  int rsp = 1;
  if (fsb != rsp){
    flag = false;
    printf("Error : f returns %d instead of %d\n", fsb, rsp);
  }

  sb[1] = 1;
  sb[3] = 1;
  sb[5] = 1;
  fsb = f_forest(sb, G);
  rsp = 4;
  if (fsb != rsp){
    flag = false;
    printf("Error : f returns %d instead of %d\n", fsb, rsp);
  }

  sb[4] = 1;
  fsb = f_forest(sb, G);
  rsp = 5;
  if (fsb != rsp){
    flag = false;
    printf("Error : f returns %d instead of %d\n", fsb, rsp);
  }

  sb[2] = 1;
  fsb = f_forest(sb, G);
  rsp = -1;
  if (fsb != rsp){
    flag = false;
    printf("Error : f returns %d instead of %d\n", fsb, rsp);
  }

  sb[4] = 1;
  sb[6] = 1;
  sb[7] = 1;
  fsb = f_forest(sb, G);
  rsp = -1;
  if (fsb != rsp){
    flag = false;
    printf("Error : f returns %d instead of %d\n", fsb, rsp);
  }
  if(flag){
    printf("Every simple test on function f passed\n");
  }
}

*/
void test_f_tree(){
  MyGraph G =  MyGraph(8);
  G.add_edge(1,2);
  G.add_edge(3,2);
  G.add_edge(1,3);
  G.add_edge(4,2);
  G.add_edge(4,5);
  G.add_edge(5,3);
  G.add_edge(6,7);
  G.add_edge(8,7);
  G.add_edge(6,8);

  bool* sb = new bool[8];
  for(int i=0; i<G.getn(); i++){
    sb[i] = 0;
  }
  int fsb = f_tree(sb, G);
  int flag = true;
  if (fsb != 0){
    flag = false;
    printf("Error : f returns %d for empty subgraph\n", fsb);
  }

  printf("\n");
  sb[0] = 1;
  fsb = f_tree(sb, G);
  int rsp = 1;
  if (fsb != rsp){
    flag = false;
    printf("Error : f returns %d instead of %d\n", fsb, rsp);
  }

  sb[1] = 1;
  sb[3] = 1;
  sb[5] = 1;
  fsb = f_tree(sb, G);
  rsp = 3;
  if (fsb != rsp){
    flag = false;
    printf("Error : f returns %d instead of %d\n", fsb, rsp);
  }

  sb[4] = 1;
  fsb = f_tree(sb, G);
  rsp = 4;
  if (fsb != rsp){
    flag = false;
    printf("Error : f returns %d instead of %d\n", fsb, rsp);
  }

  sb[2] = 1;
  fsb = f_tree(sb, G);
  rsp = -1;
  if (fsb != rsp){
    flag = false;
    printf("Error : f returns %d instead of %d\n", fsb, rsp);
  }

  sb[2] = 0;
  sb[6] = 1;
  fsb = f_tree(sb, G);
  rsp = 4;
  if (fsb != rsp){
    flag = false;
    printf("Error : f returns %d instead of %d\n", fsb, rsp);
  }



  for(int i=0; i<G.getn();i++){
    sb[i] = 0;
  }
  sb[0] = 1;
  sb[3] = 1;
  sb[4] = 1;
  sb[5] = 1;
  sb[7] = 1;
  sb[6] = 1;
  fsb = f_tree(sb, G);
  rsp = -1;
  if (fsb != rsp){
    flag = false;
    printf("Error : f returns %d instead of %d HERE\n", fsb, rsp);
  }


  for(int i=0; i<G.getn();i++){
    sb[i] = 0;
  }
  sb[0] = 1;
  sb[3] = 1;
  sb[4] = 1;
  sb[5] = 1;
  //sb[7] = 1;
  sb[6] = 1;
  fsb = f_tree(sb, G);
  rsp = 2;
  if (fsb != rsp){
    flag = false;
    printf("Error : f returns %d instead of %d\n", fsb, rsp);
  }


  if(flag){
    printf("Every simple test on function f passed\n");
  }
}



/*************************************************************************/

/*
void test_neighbours(){





  int n = 8;
  MyGraph G =  MyGraph(n);
  G.add_edge(1,2);
  G.add_edge(3,2);
  G.add_edge(1,3);
  G.add_edge(4,2);
  G.add_edge(4,5);
  G.add_edge(5,3);
  G.add_edge(6,7);
  G.add_edge(8,7);
  G.add_edge(6,8);

  int* sb = new int[n];
  sb[0] = 1;
  sb[1] = 1;
  sb[3] = 1;
  sb[5] = 1;
  int** neigh = new int*[n];
  for (int i=0; i<n;i++){
    neigh[i] = new int[n];
  }
  neighbours_forest(sb, G, neigh, 4);
  int n1[n] = {1,1,1,1,0,1,0,0};
  int n2[n] = {1,1,0,1,1,1,0,0};
  int n3[n] = {1,1,0,1,0,1,1,0};
  int n4[n] = {1,1,0,1,0,1,0,1};
  int n5[n] = {0,1,0,1,0,1,0,0};
  int n6[n] = {1,0,0,1,0,1,0,0};
  int n7[n] = {1,1,0,0,0,1,0,0};
  int n8[n] = {1,1,0,1,0,0,0,0};


  printf("Neighbour : \n");
  for(int i=0; i<n; i++){
    for(int j=0; j<n; j++){
      printf("%d\t",neigh[i][j]);
    }
    printf("\n");
  }
  printf("\n");


  int flag = true;
  int break_flag = true;
  for(int i=0; i<n && flag; i++){
    break_flag = true;
    for(int j=0; j<n && break_flag; j++){
      if(n1[j] != neigh[i][j]){
        break_flag = false;
      }
    }
    if(break_flag){
      flag = false;
    }
  }
  if(! flag){
    printf("Found n1\n");
  }

  flag = true;
  break_flag = true;
  for(int i=0; i<n && flag; i++){
    break_flag = true;
    for(int j=0; j<n && break_flag; j++){
      if(n2[j] != neigh[i][j]){
        break_flag = false;
      }
    }
    if(break_flag){
      flag = false;
    }
  }
  if(! flag){
    printf("Found n2\n");
  }



  flag = true;
  break_flag = true;
  for(int i=0; i<n && flag; i++){
    break_flag = true;
    for(int j=0; j<n && break_flag; j++){
      if(n3[j] != neigh[i][j]){
        break_flag = false;
      }
    }
    if(break_flag){
      flag = false;
    }
  }
  if(! flag){
    printf("Found n3\n");
  }




  flag = true;
  break_flag = true;
  for(int i=0; i<n && flag; i++){
    break_flag = true;
    for(int j=0; j<n && break_flag; j++){
      if(n4[j] != neigh[i][j]){
        break_flag = false;
      }
    }
    if(break_flag){
      flag = false;
    }
  }
  if(! flag){
    printf("Found n4\n");
  }



  flag = true;
  break_flag = true;
  for(int i=0; i<n && flag; i++){
    break_flag = true;
    for(int j=0; j<n && break_flag; j++){
      if(n5[j] != neigh[i][j]){
        break_flag = false;
      }
    }
    if(break_flag){
      flag = false;
    }
  }
  if(! flag){
    printf("Found n5\n");
  }







  flag = true;
  break_flag = true;
  for(int i=0; i<n && flag; i++){
    break_flag = true;
    for(int j=0; j<n && break_flag; j++){
      if(n6[j] != neigh[i][j]){
        break_flag = false;
      }
    }
    if(break_flag){
      flag = false;
    }
  }
  if(! flag){
    printf("Found n6\n");
  }



  flag = true;
  break_flag = true;
  for(int i=0; i<n && flag; i++){
    break_flag = true;
    for(int j=0; j<n && break_flag; j++){
      if(n7[j] != neigh[i][j]){
        break_flag = false;
      }
    }
    if( break_flag){
      flag = false;
    }
  }
  if(! flag){
    printf("Found n7\n");
  }



  flag = true;
  break_flag = true;
  for(int i=0; i<n && flag; i++){
    break_flag = true;
    for(int j=0; j<n && break_flag; j++){
      if(n8[j] != neigh[i][j]){
        break_flag = false;
      }
    }
    if(break_flag){
      flag = false;
    }
  }
  if(! flag){
    printf("Found n8\n");
  }




}
*/

/*************************************************************************/

/*
void test_is_tabu(){
  int n = 8;
  MyGraph G =  MyGraph(n);
  G.add_edge(1,2);
  G.add_edge(3,2);
  G.add_edge(1,3);
  G.add_edge(4,2);
  G.add_edge(4,5);
  G.add_edge(5,3);
  G.add_edge(6,7);
  G.add_edge(8,7);
  G.add_edge(6,8);

  int* sb = new int[n];
  sb[0] = 1;
  sb[1] = 1;
  sb[3] = 1;
  sb[5] = 1;
  int** neigh = new int*[n];
  for (int i=0; i<n;i++){
    neigh[i] = new int[n];
  }
  neighbours_forest(sb, G, neigh, 4);

  int n1[n] = {1,1,1,1,0,1,0,0};
  int n2[n] = {1,1,0,1,1,1,0,0};
  int n3[n] = {1,1,0,1,0,1,1,0};
  int n4[n] = {1,1,0,1,0,1,0,1};
  int n5[n] = {0,1,0,1,0,1,0,0};
  int n6[n] = {1,0,0,1,0,1,0,0};
  int n7[n] = {1,1,0,0,0,1,0,0};
  int n8[n] = {1,1,0,1,0,0,0,0};

  int flag = false;

  if(! is_tabu_forest(n1, neigh, n, G))
  {
    printf("Error : n1 is not tabu\n");
    flag = true;
  }

  if(! is_tabu_forest(n2, neigh, n, G))
  {
    printf("Error : n2 is not tabu\n");
    flag = true;
  }

  if(! is_tabu_forest(n3, neigh, n, G))
  {
    printf("Error : n3 is not tabu\n");
    flag = true;
  }


  if(! is_tabu_forest(n4, neigh, n, G))
  {
    printf("Error : n4 is not tabu\n");
    flag = true;
  }

  if(! is_tabu_forest(n5, neigh, n, G))
  {
    printf("Error : n5 is not tabu\n");
    flag = true;
  }


  if(! is_tabu_forest(n6, neigh, n, G))
  {
    printf("Error : n6 is not tabu\n");
    flag = true;
  }


  if(! is_tabu_forest(n7, neigh, n, G))
  {
    printf("Error : n7 is not tabu\n");
    flag = true;
  }


  if(! is_tabu_forest(n8, neigh, n, G))
  {
    printf("Error : n8 is not tabu\n");
    flag = true;
  }

  if(!flag){
    printf("Every simple test on is_tabu passed\n");
  }

}
*/
/*************************************************************************/



void test_tabu_search(){
  int n = 8;
  MyGraph G =  MyGraph(n);
  G.add_edge(1,2);
  G.add_edge(3,2);
  G.add_edge(1,3);
  G.add_edge(4,2);
  G.add_edge(4,5);
  G.add_edge(5,3);
  G.add_edge(6,7);
  G.add_edge(8,7);
  G.add_edge(6,8);

  bool* sb = new bool[n];
  for(int i=0;i<n;i++){
    sb[i] = 0;
  }
  sb[0] = 1;
  printf("Starting test\n");

  bool* s_star = new bool [n];
  int f_star = tabu_forest(sb, G,10, 10, s_star);
  printf("forest : f_star = %d (expected %d)\n",f_star, 6 );
  for(int i=0; i<8;i++){
    printf("%d\t",s_star[i]);
  }
  printf("\n");
  int tree = f_tree(s_star, G);

  f_star = tabu_tree(sb, G,10, 10, s_star);
  printf("forest : f_star = %d (from tree), %d (from forest), (expected %d)\n",f_star, tree, 4 );

  int* sb2 = new int[n];
  for(int i=0;i<n;i++){
    sb2[i] = 0;
  }
  sb2[0] = 1;

  int* s_star2 = new int[n];
  for(int i=0; i<G.getn();i++){
    printf("%d\t", sb2[i]);
  }
  printf("\n");
  printf("f(sb2) = %d\n", f_bipartite2(sb2,G));

  int f_star2 = tabu_bipartite2(sb2, G,5, 5, s_star2);
  printf("forest : f_star = %d (expected %d)\n",f_star2, 6 );

}


void test_cyclic(){
  int n = 8;
  MyGraph G =  MyGraph(n);
  G.add_edge(1,2);
  G.add_edge(3,2);
  G.add_edge(1,3);
  G.add_edge(4,2);
  G.add_edge(4,5);
  G.add_edge(5,3);
  G.add_edge(6,7);
  G.add_edge(8,7);
  G.add_edge(6,8);

  printf("Expected 0: %d\n",G.isCyclic());
  bool * sb = new bool [n];
  for(int i=0; i<n;i++){
    sb[i] = 0;
  }
  printf("Expected -1: %d\n",G.isCyclic(sb));

  for(int i=0; i<n;i++){
    sb[i] = 1;
  }
  sb[6] = 0;
  printf("Expected 0: %d\n",G.isCyclic(sb));



}



/*************************************************************************/





void split(const char* str, const char d, char** into)
{
    if(str != NULL && into != NULL)
    {
        int n = 0;
        int c = 0;
        for(int i = 0; str[c] != '\0'; i++,c++)
        {
            into[n][i] = str[c];
            if(str[c] == d)
            {
                into[n][i] = '\0';
                i = -1;
                ++n;
            }
        }
    }
}

void test_read_file(){

  MyGraph G = MyGraph(0);
  int m =0;
  char const* filename = "Python/Instances/DSJC125.1.txt";
  FILE* fp = fopen(filename, "r");
  if (fp == NULL){
        printf("Could not open file %s\n", filename);
    }

  char* line = NULL;
  size_t len = 0;
  while ((getline(&line, &len, fp)) != -1) {
      // using printf() in all tests for consistency
      if(line[0] == 'p'){
        // create graph
        char ** into = new char*[4];
        into[0] = new char[2];
        into[1] = new char[5];
        into[2] = new char[5];
        into[3] = new char[10];
        split(line, ' ', into);
        G =  MyGraph(atoi(into[2]));
        m = atoi(into[3]);
      //  printf("Graph created with %d vertices\n",G.getn() );
      }
      else if(line[0] == 'e'){
        // add edge
        char ** into = new char*[3];
        into[0] = new char[2];
        into[1] = new char[5];
        into[2] = new char[5];
        split(line,' ', into);
        G.add_edge(atoi(into[1]), atoi(into[2]));
      }
  }
  fclose(fp);
  if (line){
    free(line);
  }

//  printf("number of edges is %d, should be %d\n",G.getm(),m);
  int n = G.getn();
  bool* s_star = new bool [n];
  for(int i=0; i<G.getn(); i++){
    s_star[i] = 0;
  }
  init_forest(G);
  bool * s_start = new bool[n];
  bool * tmp = G.getsubgraph();
  for(int i=0; i<n;i++){
    //printf("In test : subgraph[%d] = %d\n",i,tmp[i]);
    s_start[i] = tmp[i];
  //  printf("In test : s_start[%d] = %d\n",i,s_start[i]);
  }

  int nit = 10;
  printf("Forest start:  %d\n",f_forest(s_start, G) );
  int f_star = tabu_forest(s_start, G,nit, nit, s_star);
  printf("Forest :  %d\n",f_star );

  for(int i=0; i<G.getn(); i++){
    printf("%d\t",s_star[i]);
  }
  printf("\n");

  init_tree(G);
  tmp = G.getsubgraph();
  for(int i=0; i<n;i++){
    s_start[i] = tmp[i];
  }
  printf("Tree from forest : %d\n",f_tree(s_star, G));
  f_star = tabu_tree(s_start, G, nit,  nit, s_star);
  printf("Tree : %d\n",f_star );


  int *s_start2 = new int[n];
  int *s_star2 = new int[n];
  for(int i=0; i<n;i++){
    s_start2[i] = 0;
    s_star2[i] = 0;
  }
  nit = 10;
  init_bipartite(G, s_start2);
  bool* subG = G.getsubgraph();
  //for(int i=0; i<n;i++){
    //printf("In test : subG[%d] = %d , s_start2[%d] = %d\n",i,subG[i],i,s_start2[i]);
  //}
  f_star = tabu_bipartite2(s_start2, G, nit, nit, s_star2);
  printf("Bipartite : %d\n",f_star );

  /*
  n = 8;
  MyGraph G2 =  MyGraph(n);
  G2.add_edge(1,2);
  G2.add_edge(3,2);
  G2.add_edge(1,3);
  G2.add_edge(4,2);
  G2.add_edge(4,5);
  G2.add_edge(5,3);
  G2.add_edge(6,7);
  G2.add_edge(8,7);
  G2.add_edge(6,8);

  int* sb = new int[n];
  for(int i=0;i<n;i++){
    sb[i] = 0;
  }
  sb[0] = 1;

  int nb_it = 250;
  int f_star2 = tabu_forest(sb, G2,nb_it, nb_it, s_star);
  printf("Forest :  %d (best is %d)\n",f_star2 , 6);
  f_star2 = tabu_tree(sb, G2,nb_it, nb_it, s_star);
  printf("Tree : %d (best is %d)\n",f_star2 , 4);
  f_star2 = tabu_bipartite(sb, G2,nb_it, nb_it, s_star);
  printf("Bipartite : %d (best is %d)\n",f_star2, 6 );
  f_star2 = tabu_bipartite2(sb, G2,nb_it, nb_it, s_star);
  printf("Bipartite : %d (best is %d)\n",f_star2, 6 );
  */
}



/*****************************************************************************/




/*

void test_neighbours_bipartite2(){
  int n = 8;
  MyGraph G2 =  MyGraph(n);
  G2.add_edge(1,2);
  G2.add_edge(3,2);
  G2.add_edge(1,3);
  G2.add_edge(4,2);
  G2.add_edge(4,5);
  G2.add_edge(5,3);
  G2.add_edge(6,7);
  G2.add_edge(8,7);
  G2.add_edge(6,8);

  int * s = new int[n];
  s[0] = 1;
  s[1] = 2;
  s[2] = 0;
  s[3] = 1;
  s[4] = 2;
  s[5] = 0;
  s[6] = 1;
  s[7] = 0;

  int** nei = new int*[n];
  for(int i=0; i<n;i++){
    nei[i] = new int[n];
    for(int j=0;j<n;j++){
      nei[i][j] = 0;
    }
  }

  neighbours_bipartite2(s, G2, nei, 5);

  for(int i=0; i<n;i++){
    printf("%d\t",s[i]);
  }
  printf("\n\n");


  for(int i=0; i<n;i++){
    for(int j=0; j<n;j++){
      printf("%d\t",nei[i][j]);
    }
    printf("\n");
  }

  int* sb = new int[n];
  int* s_star = new int[n];
  for(int i=0;i<n;i++){
    sb[i] = 0;
  }
  sb[0] = 1;
  int nb_it = 250;
  int f_star2 = tabu_bipartite2(sb, G2,nb_it, nb_it, s_star);
  printf("Bipartite : %d (best is %d)\n",f_star2, 6 );


}





void test_neighbours_forest(){
  int n = 8;
  MyGraph G2 =  MyGraph(n);
  G2.add_edge(1,2);
  G2.add_edge(3,2);
  G2.add_edge(1,3);
  G2.add_edge(4,2);
  G2.add_edge(4,5);
  G2.add_edge(5,3);
  G2.add_edge(6,7);
  G2.add_edge(8,7);
  G2.add_edge(6,8);

  int * s = new int[n];
  s[0] = 1;
  s[1] = 0;
  s[2] = 1;
  s[3] = 1;
  s[4] = 1;
  s[5] = 0;
  s[6] = 1;
  s[7] = 0;

  int** nei = new int*[n];
  for(int i=0; i<n;i++){
    nei[i] = new int[n];
    for(int j=0;j<n;j++){
      nei[i][j] = 0;
    }
  }

  neighbours_forest(s, G2, nei, 5);

  for(int i=0; i<n;i++){
    printf("%d\t",s[i]);
  }
  printf("\n\n");


  for(int i=0; i<n;i++){
    for(int j=0; j<n;j++){
      printf("%d\t",nei[i][j]);
    }
    printf("\n");
  }

  int* sb = new int[n];
  int* s_star = new int[n];
  for(int i=0;i<n;i++){
    sb[i] = 0;
  }
  sb[0] = 1;
  int nb_it = 250;
  int f_star2 = tabu_bipartite2(sb, G2,nb_it, nb_it, s_star);
  printf("Bipartite : %d (best is %d)\n",f_star2, 6 );


}





void test_isConnected(){
  int n = 8;
  MyGraph G =  MyGraph(n);
  G.add_edge(1,2);
  G.add_edge(3,2);
  G.add_edge(1,3);
  G.add_edge(4,2);
  G.add_edge(4,5);
  G.add_edge(5,3);
  G.add_edge(6,7);
  G.add_edge(8,7);
  G.add_edge(6,8);

  int * s = new int[n];
  int* size = new int[n];
  for(int i=0; i<n;i++){
    size[i] = 0;
    s[i] = 0;
  }


  int flag;

  flag = G.isConnected(s, size);
  printf("Is connected : %d (should be 1)\n", flag);
  printf("Is connected : size =\n");
  for(int i=0; i<n;i++){
    printf("%d\t",size[i] );
  }
  printf("\n");

  for(int i=0; i<n;i++){
    size[i] = 0;
  }
  s[0] = 1;
  flag = G.isConnected(s, size);
  printf("Is connected : %d (should be 1)\n", flag);
  printf("Is connected : size =\n");
  for(int i=0; i<n;i++){
    printf("%d\t",size[i] );
  }
  printf("\n");


  for(int i=0; i<n;i++){
    size[i] = 0;
  }
  s[1] = 1;
  flag = G.isConnected(s, size);
  printf("Is connected : %d (should be 1)\n", flag);
  printf("Is connected : size =\n");
  for(int i=0; i<n;i++){
    printf("%d\t",size[i] );
  }
  printf("\n");



  for(int i=0; i<n;i++){
    size[i] = 0;
  }
  s[4] = 1;
  flag = G.isConnected(s, size);
  printf("Is connected : %d (should be 0)\n", flag);
  printf("Is connected : size =\n");
  for(int i=0; i<n;i++){
    printf("%d\t",size[i] );
  }
  printf("\n");


  for(int i=0; i<n;i++){
    size[i] = 0;
  }
  s[3] = 1;
  flag = G.isConnected(s, size);
  printf("Is connected : %d (should be 1)\n", flag);
  printf("Is connected : size =\n");
  for(int i=0; i<n;i++){
    printf("%d\t",size[i] );
  }
  printf("\n");




  for(int i=0; i<n;i++){
    size[i] = 0;
  }
  s[5] = 1;
  s[6] = 1;
  flag = G.isConnected(s, size);
  printf("Is connected : %d (should be 1)\n", flag);
  printf("Is connected : size =\n");
  for(int i=0; i<n;i++){
    printf("%d\t",size[i] );
  }
  printf("\n");




  for(int i=0; i<n;i++){
    size[i] = 0;
    s[i] = 0;
  }
  s[0] = 1;
  s[3] = 1;
  s[4] = 1;
  s[5] = 1;
  s[7] = 1;
  s[6] = 1;
  flag = G.isConnected(s, size);
  printf("Is connected : %d (should be 1)\n", flag);
  printf("Is connected : size =\n");
  for(int i=0; i<n;i++){
    printf("%d\t",size[i] );
  }
  printf("\n");




}







*/


void mod_matrix(int*** mat, int r, int c){
  //printf("%p and %p and %p\n", &mat, mat, &mat[0][0]);
  for(int i=0; i<r; i++){
    for(int j=0; j<c; j++){
      (*mat)[i][j] = 2+ (*mat)[i][j];
    }
  }

}


void test_matrix(){

  int r = 5;
  int c = 4;
  int** mat = (int**) malloc(r*sizeof(int*));
  for(int i=0;i<r;i++){
    mat[i] = (int*) malloc(c*sizeof(int));
    for(int j=0;j<c;j++){
      mat[i][j] = j;
    }
  }

  for(int i=0; i<r; i++){
    for(int j=0; j<c; j++){
      printf("%d\t",mat[i][j]);
    }
    printf("\n");
  }
  printf("%p and %p and %p\n", &mat, mat, &mat[0][0]);
  mod_matrix(&mat, r, c);
  printf("%p and %p and %p\n", &mat, mat, &mat[0][0]);
  for(int i=0; i<r; i++){
    for(int j=0; j<c; j++){
      printf("%d\t",mat[i][j]);
    }
    printf("\n");
  }
}




/******************************************************************************
*
*
*
*       Main function
*
*
*
*******************************************************************************/








int main(int argc, char const *argv[]) {
  //test_subgraph();
  //test_f_forest();
    //test_f_tree();
  //test_isConnected();
  //test_neighbours();
  //test_is_tabu();
  //test_tabu_search();
  test_read_file();
  //test_initial_bipartite();
  //test_neighbours_bipartite2();
  //test_neighbours_forest();

  //test_matrix();
  //test_tabu_search();
  //test_cyclic();
}
