#include <iostream>
#include <algorithm>
#include "MyGraph.h"
#include "InitSol.h"



int f_bipartite2(int*, MyGraph);
int neighbours_bipartite2(int*, MyGraph, int);
int max_f_bipartite2(int* , MyGraph, int,   bool**, int);
int is_tabu_bipartite2(int *, int**, int, MyGraph);
int tabu_bipartite2(int * , MyGraph , int , int , int * );
