#include <iostream>
#include <algorithm>
#include "MyGraph.h"
#include "InitSol.h"
#include "TabuForest.h"



int f_tree(bool*, MyGraph);
int neighbours_tree(bool*, MyGraph, bool**, int);
int max_f_tree(bool*, MyGraph, bool**, int,   bool**, int, bool*);
int is_tabu_tree(bool*, bool**, int, MyGraph);
int tabu_tree(bool * , MyGraph , int , int , bool * );
