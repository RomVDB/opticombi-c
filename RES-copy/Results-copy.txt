===========================================================================
Forest
===========================================================================
iteration = 1000; repetition = 5
Python/Instances/DSJC125.1.txt & 41.6 & 53 & 56 & 55 & 8.33149e+07


iteration = 1000; repetition = 5
Python/Instances/DSJC125.5.txt & 10 & 14 & 15 & 14.2 & 3.60266e+07


iteration = 1000; repetition = 5
Python/Instances/DSJC125.9.txt & 4 & 6 & 6 & 6 & 7.20791e+07


iteration = 500; repetition = 5
Python/Instances/DSJC250.1.txt & 48 & 64 & 68 & 66.2 & 3.09839e+08

iteration = 500; repetition = 5
Python/Instances/DSJC250.5.txt & 11.4 & 15 & 17 & 16 & 6.06637e+07

iteration = 500; repetition = 5
Python/Instances/DSJC250.9.txt & 4.6 & 7 & 7 & 7 & 6.08272e+07

Python/Instances/DSJC500.1.txt & 58.6 & 73 & 76 & 75 & 876.847865

iteration = 250; repetition = 3
Python/Instances/DSJC500.5.txt & 12 & 17 & 18 & 17.3333 & 1.14853e+08

iteration = 250; repetition = 3
Python/Instances/DSJC500.9.txt & 5 & 7 & 7 & 7 & 7.69601e+07


iteration = 100; repetition = 1
Python/Instances/DSJC1000.1.txt & 65 & 83 & 83 & 83 & 1.77251e+09



iteration = 250; repetition = 3
Python/Instances/DSJC1000.9.txt & 12 & 19 & 19 & 19 & 414.716701

Python/Instances/DSJC1000.5.txt	 init : 12.000000, worst : 19, best : 19, avg : 19.000000, time : 414.716701
Python/Instances/DSJC1000.5.txt	 init : 12.000000, worst : 19, best : 19, avg : 19.000000, time : 409312454.000000



iteration = 250; repetition = 3
Python/Instances/DSJC1000.9.txt & 5 & 7 & 7 & 7 & 7.37285e+08





===========================================================================
Tree
===========================================================================
iteration = 1000; repetition = 5
Python/Instances/DSJC125.1.txt & 33.8 & 46 & 49 & 47.8 & 1.08987e+08

iteration = 1000; repetition = 5
Python/Instances/DSJC125.5.txt & 9.4 & 13 & 14 & 13.2 & 4.93797e+07

iteration = 1000; repetition = 5
Python/Instances/DSJC125.9.txt & 4 & 6 & 6 & 6 & 8.47515e+07 

iteration = 500; repetition = 5
Python/Instances/DSJC250.1.txt & 42.6 & 53 & 57 & 54.4 & 3.03398e+08 

iteration = 500; repetition = 5
Python/Instances/DSJC250.5.txt & 11.2 & 14 & 16 & 15.2 & 7.50061e+07

iteration = 500; repetition = 5
Python/Instances/DSJC250.9.txt & 4.6 & 6 & 7 & 6.6 & 8.3321e+07

iteration = 250; repetition = 2
Python/Instances/DSJC500.1.txt & 54.4 & 65 & 66 & 65.6 & 859.110525 

iteration = 250; repetition = 5
Python/Instances/DSJC500.5.txt & 12.2 & 15 & 16 & 15.8 & 1.27646e+08 

iteration = 250; repetition = 5
Python/Instances/DSJC500.9.txt & 5 & 6 & 7 & 6.6 & 9.80241e+07 


DSJC1000.1.txt & 61 & 70 & 70 & 70 & 1638.15 

iteration = 250; repetition = 2
Python/Instances/DSJC1000.5.txt & 12 & 17 & 17 & 17 & 483.269508

iteration = 250; repetition = 2
Python/Instances/DSJC1000.9.txt & 5 & 7 & 7 & 7 & 385.644






===========================================================================
Bipartite
===========================================================================
iteration = 1000; repetition = 5
Python/Instances/DSJC125.1.txt & 50.2 & 61 & 66 & 63.6 & 1.75276e+07


iteration = 1000; repetition = 5
Python/Instances/DSJC125.5.txt & 13.4 & 19 & 19 & 19 & 1.96606e+07

iteration = 1000; repetition = 5
Python/Instances/DSJC125.9.txt & 5.4 & 8 & 8 & 8 & 2.97944e+07

iteration = 500; repetition = 5
Python/Instances/DSJC250.1.txt & 60.8 & 76 & 79 & 77.8 & 3.30998e+07

iteration = 500; repetition = 5
Python/Instances/DSJC250.5.txt & 15.4 & 20 & 22 & 21.2 & 2.64781e+07

iteration = 500; repetition = 5
Python/Instances/DSJC250.9.txt & 6 & 9 & 10 & 9.2 & 1.96269e+07


iteration = 500; repetition = 5
Python/Instances/DSJC500.1.txt & 75.6 & 90 & 99 & 96 & 1.73497e+08

iteration = 500; repetition = 5
Python/Instances/DSJC500.5.txt & 18 & 22 & 25 & 23.2 & 9.62978e+07

iteration = 500; repetition = 5
Python/Instances/DSJC500.9.txt & 6.4 & 8 & 10 & 9.2 & 1.03973e+08

iteration = 250; repetition = 3
Python/Instances/DSJC1000.1.txt & 90 & 105 & 117 & 113 & 3.98483e+08

iteration = 250; repetition = 3
Python/Instances/DSJC1000.5.txt & 18.6667 & 24 & 26 & 25 & 1.63896e+08


iteration = 250; repetition = 3
Python/Instances/DSJC1000.9.txt & 7 & 9 & 10 & 10.8 & 1.0709816e+08


