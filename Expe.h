#include <iostream>
#include <fstream>
#include <sstream> 
#include <string>
#include "MyGraph.h"
#include "TabuForest.h"
#include "TabuTree.h"
#include "TabuBipartite.h"
#include "TabuBipartite2.h"
#include "InitSol.h"

#include <chrono>
using namespace std::chrono;


void split(const char*, const char , char**);
