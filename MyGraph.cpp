#include <iostream>
#include "MyGraph.h"
using namespace std;


//class MyGraph
//{


  MyGraph::MyGraph(int n){
    this->n = n;
    this->m = 0;
    this->adjmatrix = new bool*[n];
    this->subgraph = new bool[n];
    int i;
    for(i=0;i<n;i++){
      this->adjmatrix[i] = new bool[n];
      for(int j=0; j<n;j++){
        this->adjmatrix[i][j] = 0;
      }
      this->subgraph[i] = 0;
    }

  }

  MyGraph::~MyGraph(){
    /*free(this->subgraph);
    for(int i=0;i<n;i++){
      free(this->adjmatrix[i]);
    }
    free(this->adjmatrix);*/
  }

  int MyGraph::getn(){
    return n;
  }
  int MyGraph::getm(){
    return m;
  }
  bool** MyGraph::getadjmatrix(){
    return this->adjmatrix;
  }
  bool* MyGraph::getsubgraph(){
    return this->subgraph;
  }

  /**
  * add_edge add an edge between vertices v1 and v2 if there are not an edge already
  * v1, v2 = 1,...,n
  */
  int MyGraph::add_edge(int v1, int v2){
    if(v1 > this->n || v2 > this->n){
      printf("ERROR : trying to add edge between %d and %d while there are %d vertices\n", v1, v2, this->n);
      return -1;
    }

    if(this->adjmatrix[v1-1][v2-1] == 1){
      printf("ERROR : already an edge between %d and %d\n", v1, v2);
      return -1;

    }
    else{
      this->m ++;
      this->adjmatrix[v1-1][v2-1] = 1;
      this->adjmatrix[v2-1][v1-1] = 1;
    }
    return 0;
  }


  int MyGraph::remove_edge(int v1, int v2){
    if(v1 > this->n || v2 > this->n){
      printf("ERROR : trying to remove and edge between %d and %d while there are %d vertices\n", v1, v2, this->n);
      return -1;
    }

    if(this->adjmatrix[v1-1][v2-1] == 0){
      printf("ERROR : no edge to remove between %d and %d\n", v1, v2);
      return -1;

    }
    else{
      this->m --;
      this->adjmatrix[v1-1][v2-1] = 0;
      this->adjmatrix[v2-1][v1-1] = 0;
    }
    return 0;

  }


  /*
  * add vertex v into subgraph if possible
  *
  *
  */
  int MyGraph::subgraph_addvertex(int v){
    if(v > this-> n || v <= 0){
      printf("Error : trying to add vertex %d into subgraph while there are %d vertices in full graph\n", v, this->n );
      return -1;
    }
    if(this->subgraph[v-1] == 1){
      printf("Error : trying to add vertex %d already in subgraph\n", v);
      return -1;
    }
    this->subgraph[v-1] = 1;
    return 0;
  }

  /*
  * remove vertex v from subgraph if possible
  *
  *
  */
  int MyGraph::subgraph_removevertex(int v){
    if(v > this-> n || v <= 0){
      printf("Error : trying to remove vertex %d into subgraph while there are %d vertices in full graph\n", v, this->n );
      return -1;
    }
    if(this->subgraph[v-1] == 0){
      printf("Error : trying to remove vertex %d already absent from subgraph\n", v);
      return -1;
    }
    this->subgraph[v-1] = 0;
    return 0;
  }


  int MyGraph::create_subgraph(bool* sub){
    int len_sub = sizeof(sub)/sizeof(sub[0]);
    if (this->n  != len_sub){
      printf("Error : subgraph of size %d while graph of size %d \n",len_sub, this->n);
      return -1;
    }
    int i;
    for(i = 0; i< this->n; i++){
          this->subgraph[i] = sub[i];
    }
    return 0;
  }

  /*
  * isCyclic : return true if graph contains cycle, false otherwise
  * sub = 0 : consider full graph,
  * sub = 1 : consider subgraph.
  */
  int MyGraph::isCyclic(int sub){
    if(sub == 0){
      bool* visited = new bool[this->n];
      for(int i=0; i<this->n; i++){
        visited[i] = 0;
      }
      for(int i=0; i< this->n; i++){
        if (visited[i] == 0){
          int ret = isCyclicUtil(i,visited,-1,sub, NULL);
          if(ret != -1){
            return i;
          }
        }
      }
      return -1;
    }
    else{
      bool* visited = new bool[this->n];
      for(int i=0; i<this->n; i++){
        visited[i] = 0;
      }
      for(int i=0; i< this->n; i++){
        if (visited[i] == 0 && this->subgraph[i] == 1){
          int ret = isCyclicUtil(i,visited,-1,sub, this->subgraph);
          if(ret != -1){
            return i;
          }
        }
      }
      return -1;

    }
  }




  /*
  * isCyclic : return true if graph contains cycle, false otherwise
  * sub = 0 : consider full graph,
  * sub = 1 : consider subgraph.
  */
  int MyGraph::isCyclic(bool* subgraph, int start){
    if (start == -1){
      bool* visited = new bool[this->n];
      for(int i=0; i<this->n; i++){
        visited[i] = 0;
      }
      for(int i=0; i< this->n; i++){
        if (visited[i] == 0 && subgraph[i] != 0){
          int ret = isCyclicUtil(i,visited,-1,1,subgraph);
          if(ret != -1){
            return ret;
          }
        }
      }
      return -1;
    }
    else{
      bool* visited = new bool[this->n];
      for(int i=0; i<this->n; i++){
        visited[i] = 0;
      }
      int ret = isCyclicUtil(start, visited, -1, 1, subgraph);
      if(ret != -1){
        return ret;
      }
      else{
        return isCyclic(subgraph, -1);
      }
    }
    return 0;
  }


  int MyGraph::isCyclic(bool* subgraph){
    return isCyclic(subgraph, -1);
  }


  /*
  * return -1 if no cycle, else return a vertex in a cycle
  *
  */
  int MyGraph::isCyclicUtil(int current, bool* visited, int parent, int sub, bool* subgraph){
    if(sub == 0){
      visited[current] = true;
      int i;
      for(int v=0; v< this->n; v++){
        i = this->adjmatrix[current][v];
        if (i ==1){
          if (visited[v] == 0){
             int ret = isCyclicUtil(v, visited, current, sub, NULL);
             if(ret != -1){
               return v;
             }
          }
          else if(v != parent){
            return v;
          }
        }
      }
      return -1;
    }
    else{
      visited[current] = true;
      int i;
      for(int v=0; v< this->n; v++){
        i = this->adjmatrix[current][v];
        if(i == 1 && subgraph[v] == 1){
          if (visited[v] == false){
            int ret = isCyclicUtil(v, visited, current, sub, subgraph);
            if(ret != -1){
            //         printf("Cycle found from %d\n",i);
              return v;
            }
          }
          else if(v != parent){
            return v;
          }
        } // end if i== 1 && subgraph[v]
      } // end for
      return -1;
    }// end else sub
  } // end function




  /*
  * isConnected : return true if graph is conncected, false otherwise
  * sub = 0 : consider full graph,
  * sub = 1 : consider subgraph.
  */
  int MyGraph::isConnected(int sub){
    bool* visited = new bool[this->n];
    for(int i=0; i<this->n; i++){
      visited[i] = 0;
    }
    if(sub == 0){
      isConnectedUtil(0,visited,-1,sub, NULL);
      for(int i=0; i<this->n; i++){
        if(visited[i] == 0){
          return false;
        }
      }
      return true;
    }
    else{
      int flag = 1;
      int start = 0;
      for(start=0; start< this->n && flag; start++){
        if(this->subgraph[start] == 1){
          flag = 0;
        }
      }
      if (start >= this->n){
        return true;
      }
      isConnectedUtil(start,visited,-1,sub, this->subgraph);
      for(int i=start; i<this->n; i++){
        if(subgraph[i] == 1){
          if(visited[i] == 0){
            return false;
          }
        }
      }
      return true;
    }
  }


  /*
  * isConnected : return true if graph is conncected, false otherwise
  * sub = 0 : consider full graph,
  * sub = 1 : consider subgraph.
  */
  int MyGraph::isConnected(bool* subgraph){
    bool* visited = new bool[this->n];
    for(int i=0; i<this->n; i++){
      visited[i] = 0;
    }

    int flag = 1;
    int start = 0;
    for(start=0; start< this->n && flag; start++){
      if(subgraph[start] == 1){
        flag = 0;
      }
    }
    if (start >= this->n){
      return true;
    }
    isConnectedUtil(start,visited,-1,1,subgraph);
    for(int i=start; i<this->n; i++){
      if(subgraph[i] == 1){
        if(visited[i] == 0){
          return false;
        }
      }
    }
    return true;
  }



  /*
  * Store size of components in size
  *
  *
  */

  int MyGraph::isConnected(bool* subgraph, int* size){
    bool* visited = new bool[this->n];
    for(int i=0; i<this->n; i++){
      visited[i] = 0;
    }


    int flag = true;
    int component_counter = 0;
    int counter = 0;
    size[0] = 0;
    int start;
    for(start=0; start < this->n ; start++){
      if(subgraph[start] == 1 && visited[start] == 0){
        // start is a vertex no yet visited in subgraph
        isConnectedUtil(start,visited,-1,1,subgraph);
        counter = 0;
        for(int i=0; i<this->n; i++){
          if(subgraph[i] == 1 && visited[i] != 0){
            // Count vertex already visited
            counter ++;
          }
          else if(subgraph[i] == 1 && visited[i] == 0){
            flag = false;
          }
        }
        if(component_counter == 0){
          size[0] = counter;
          component_counter ++;
        }
        else{
          size[component_counter] = counter;
          component_counter ++;
        }
      }
    } // for start
    // Transform size from cumulative to actual sizes

    for(int i=this->n -1 ; i>0; i--){
      if((size[i] - size[i-1]) >= 0){
        size[i] = size[i] - size[i-1];
      }
      else{
        size[i] = 0;
      }
    }
    return flag;
  }










  int MyGraph::isConnectedUtil(int current, bool* visited, int parent, int sub, bool* subgraph){
    if(sub == 0){
      visited[current] = true;
      int i;
      for(int v=0; v< this->n; v++){
        i = this->adjmatrix[current][v];
        if (i ==1){
          if (visited[v] == 0){
            isConnectedUtil(v, visited, current, sub, NULL);
          }
        }
      }
      return true;
    }
    else{
      visited[current] = true;
      int i;
      for(int v=0; v< this->n; v++){
        i = this->adjmatrix[current][v];
        if(i == 1 && subgraph[v] == 1){
          if (visited[v] == 0){
            isConnectedUtil(v, visited, current, sub, subgraph);
          }
        }
      }
      return true;
    } // if subgraph
  } // end funtion


  /*
  * isBipartite : return true if graph is bipartite, false otherwise
  * sub = 0 : consider full graph,
  * sub = 1 : consider subgraph.
  */
  int MyGraph::isBipartite(int sub){
    int* color = new int[this->n];
    bool* visited = new bool[this->n];
    for(int i=0; i<this->n; i++){
      color[i] = 0;
      visited[i] = 0;
    }
    if(sub == 0){
      for(int i=0; i< this->n; i++){
        if(visited[i] == 0){
          int ret = isBipartiteUtil(i, visited, color, sub, NULL);
          if(ret == false){
            return false;
          }
        }
      }
      return true;
    }
    else{
      bool* tmp_subgraph = new bool[this-> n];
      for(int i=0; i<this->n;i++){
        if(this->subgraph[i] == 0){
          tmp_subgraph[i] = 0;
        }
        else{
          tmp_subgraph[i] = 1;
        }
      }
      for(int i=0; i< this->n; i++){
        if(this->subgraph[i] == 1){
          if(visited[i] == 0){
            int ret = isBipartiteUtil(i, visited, color, sub, tmp_subgraph);
            if(ret == false){
              return false;
            }
          }
        }
      }
      return true;
    }
  }

  /*
  * isBipartite : return true if graph is bipartite, false otherwise
  * subgraph[i] = 1 if vertex i is in subgraph, 0 otherwise
  */
  int MyGraph::isBipartite(bool* subgraph, int * col){
    int* color = new int[this->n];
    bool* visited = new bool[this->n];
    for(int i=0; i< this->n;i++){
      color[i] = 0;
      visited[i] = 0;
    }
    for(int i=0; i< this->n; i++){
      if(subgraph[i] != 0){
        if(visited[i] == 0){
          int ret = isBipartiteUtil(i, visited, color, 1, subgraph);
          if(ret == false){
            return false;
          }
        }
      }
    }
    for(int i=0; i<this->n; i++){
      col[i] = color[i];
    }
    return true;
  }


  /*
  * isBipartite : return true if graph is bipartite, false otherwise
  * subgraph[i] = 1 if vertex i is in subgraph, 0 otherwise
  */
  int MyGraph::isBipartite(bool* subgraph){
    int* color = new int[this->n];
    bool* visited = new bool[this->n];
    for(int i=0; i< this->n;i++){
      color[i] = 0;
      visited[i] = 0;
    }
    for(int i=0; i< this->n; i++){
      if(subgraph[i] != 0){
        if(visited[i] == 0){
          int ret = isBipartiteUtil(i, visited, color, 1, subgraph);
          if(ret == false){
            return false;
          }
        }
      }
    }
    return true;
  }

  int MyGraph::isBipartiteUtil(int current, bool* visited, int* color, int sub, bool* subgraph){
    visited[current] = true;
    if(sub == 0){
      int i;
      for(int v=0; v< this->n; v++){
        i = this->adjmatrix[current][v];
        if (i != 0){
          if (visited[v] == false){
            visited[v] = true;
            color[v] = 1-color[current];
            int ret = isBipartiteUtil(v, visited, color, sub, NULL);
            if(ret == false){
              return false;
            }
          }
          else if(color[current] == color[v]){
            return false;
          }
        }
      }
      return true;

    } // end if sub == 0
    else{
      /*printf("Is bip\n");
      for(int i=0; i<this->n;i++){
        printf("%d\t", subgraph[i]);
      }
      printf("\n");*/
      int i;
      for(int v=0; v< this->n; v++){
        i = this->adjmatrix[current][v];
        if (i != 0 && subgraph[v] != 0){
          if (visited[v] == false){
            visited[v] = true;
            color[v] = 1-color[current];
            int ret = isBipartiteUtil(v, visited, color, sub, subgraph);
            if(ret == false){
              return false;
            }
          }
          else if(color[current] == color[v] && current != v){
            return false;
          }
        }
      }
      return true;
    }// end else sub
  } // end function



  int MyGraph::isCyclic(){
    return isCyclic(0);
  }
  int MyGraph::isConnected(){
    return isConnected(0);
  }
  int MyGraph::isBipartite(){
    return isBipartite(0);
  }
//}
