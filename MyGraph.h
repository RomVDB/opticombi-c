#ifndef MYGRAPH_H
#define MYGRAPH_H

using namespace std;


class MyGraph{
private:

  int n;
  int m;
  bool** adjmatrix;
  bool* subgraph;

  int isConnectedUtil(int, bool*, int, int, bool*);
  int isCyclicUtil(int, bool*, int, int, bool*);
  int isBipartiteUtil(int, bool*, int* , int, bool*);


public:

  MyGraph(int n);
  ~MyGraph();

  int getn();
  int getm();
  bool** getadjmatrix();
  bool* getsubgraph();
  int add_edge(int v1, int v2);
  int remove_edge(int v1, int v2);
  int subgraph_addvertex(int);
  int subgraph_removevertex(int);
  int create_subgraph(bool* sub);
  int isCyclic(int);
  int isConnected(int);
  int isBipartite(int);
  int isCyclic(bool*);
  int isCyclic(bool*, int);
  int isConnected(bool*);
  int isConnected(bool*, int*);
  int isBipartite(bool*, int*);
  int isBipartite(bool*);
  int isCyclic();
  int isConnected();
  int isBipartite();

};


#endif
