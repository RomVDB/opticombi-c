#include "TabuBipartite.h"
/*
*
*/
int f_bipartite(int* s, MyGraph G){
  if (G.isBipartite(s) == false ){
    return -1;
  }
  else{
    int count = 0;
    for(int i=0; i<G.getn(); i++){
      if(s[i] == 1){
        count ++;
      }
    }
    return count;
  }
}




/*
*
*/
int neighbours_bipartite(int* s, MyGraph G, int** neigh, int snb){
  // Remove a vertex in subgraph
  int glob_count = 0;
  for(glob_count=0; glob_count<snb;glob_count++){
    int line_counter = 0;
    for(int i=0; i<G.getn();i++){
      if(s[i] == 1){
        if(line_counter == glob_count){
          neigh[glob_count][i] = 0;
        }
        else{
          neigh[glob_count][i] = s[i];
        }
        line_counter++;
      }
      else{
        neigh[glob_count][i] = s[i];
      }
    }
  }
  // Add a vertex in subgraph
  glob_count = 0;
  for(glob_count=0; glob_count<G.getn()-snb ;glob_count++){
    int line_counter = 0;
    for(int i=0; i<G.getn();i++){
      if(s[i] == 0){
        if(line_counter == glob_count){
          neigh[snb+glob_count][i] = 1;
        }
        else{
          neigh[glob_count+snb][i] = s[i];
        }
        line_counter++;
      }
      else{
        neigh[glob_count+snb][i] = s[i];
      }
    }
  }

  return 0;
}




/*
*
*/
int max_f_bipartite(int* s, MyGraph G, int **neigh, int fs, int** T, int nbtabu, int * smax){
  int n = G.getn();
  //int * smax = new int[n];
  int fmax = fs;
  int nbs =0;
  for(int i=0; i< n;i++){
    if(s[i] == 1){
      nbs++;
    }
  }
  neighbours_bipartite(s, G, neigh, nbs);
  int fbar;
  int k;
  int flag = true;
  for(k=0; k<n; k++){
    if(!(is_tabu_bipartite(neigh[k], T, nbtabu, G))){
      // not tabu
      fbar = f_bipartite(neigh[k], G);
      if(fbar > fmax || flag){
        // Better solution found or copy first one
        flag = false;
        fmax = fbar;
        for(int i=0; i<n; i++){
          smax[i] = neigh[k][i];
        }
      }
    }
  }
  return fmax;
}


/*
*
*/
int is_tabu_bipartite(int* s, int** T, int nbtabu, MyGraph G){
  int break_flag = true;
  for(int i=0; i< nbtabu; i++){
    break_flag = true;
    for(int j=0; j<G.getn() && break_flag;j++){
      if(s[j] != T[i][j]){
        break_flag = false;
      }
    }
    if(break_flag){
      return true;
    }
  }
  return false;
}




/*
*
*/
int tabu_bipartite(int * s_start, MyGraph G, int kmax, int nbtabu, int * s_star){
  // Initialization
  int k;
  int n = G.getn();
  int ** T = new int*[nbtabu];
  for(int i=0; i<nbtabu; i++){
    T[i] = new int[n];
  }
  int f_star = f_bipartite(s_start, G);
  int ** neigh = new int*[n];
  int* s_prime = new int[n];
  int* old = new int[n];
  int f_prime = 0;
  for(int i=0; i<n; i++){
    neigh[i] = new int[n];
    for(int j=0;j<n;j++){
      neigh[i][j] = 0;
    }
    s_star[i] = s_start[i];
    old[i] = s_start[i];
  }

  // Iteration
  printf("Iteration %d : f_star = %d\n", 0, f_star);
  for(k=0; k < kmax; k++){

    f_prime =  max_f_bipartite(old, G, neigh, f_prime, T, nbtabu, s_prime);
    //printf("Iteration %d : f_star = %d and current is %d\n", k, f_star, f_prime);
    if(f_prime > f_star){
      // Success : update s_star and s_prime
      f_star = f_prime;
      for(int i=0; i<n; i++){
        s_star[i] = s_prime[i];
      }
    }

    // Add s_sprime in Tabu list
    for(int i=0; i<n; i++){
      T[k%nbtabu][i] = s_prime[i];
      old[i] = s_prime[i];
      //printf("%d\t",s_prime[i]);
    }
    //printf("\n");

  }
  return f_star;
}
